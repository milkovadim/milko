import java.util.*;

public final class Median {

    public static float median(int[] array){
        Arrays.sort(array);
        if(array.length%2==0){
            return ((float) (array[(array.length/2)-1]+array[array.length/2])/2);
        }
        else{
            return (float) array[array.length/2];
        }
    }
    public static double median(double[] array){
        Arrays.sort(array);
        if(array.length%2==0){
            return ((array[(array.length/2)-1]+array[array.length/2]) /2);
        }
        else{
            return array[array.length/2];
        }
    }
}
