package Bank;
import org.junit.Assert;
import org.junit.Test;
import java.io.ByteArrayInputStream;
import java.math.BigDecimal;


public class ATMTest {

    @Test
    public void moneyAddIntCorrectly() {
        String userInput  = "50";
        ByteArrayInputStream inputStream = new ByteArrayInputStream(userInput .getBytes());
        System.setIn(inputStream);
        ATM atm = new ATM();
        BigDecimal actual = atm.moneyAdd();
        int index = actual.compareTo(new BigDecimal(50));
        Assert.assertEquals(0,index);
    }

    @Test
    public void moneyAddDoubleCorrectly() {
        String userInput  = "50.1";
        ByteArrayInputStream inputStream = new ByteArrayInputStream(userInput .getBytes());
        System.setIn(inputStream);
        ATM atm = new ATM();
        BigDecimal actual = atm.moneyAdd();
        int index = new BigDecimal("50.1").compareTo(actual);
        Assert.assertEquals(0,index);
    }

    @Test (expected = IllegalArgumentException.class)
    public void moneyAddFormatExceptionString() {
        String userInput  = "vadim";
        ByteArrayInputStream inputStream = new ByteArrayInputStream(userInput .getBytes());
        System.setIn(inputStream);
        ATM atm = new ATM();
        BigDecimal actual = atm.moneyAdd();
    }

    @Test (expected = IllegalArgumentException.class)
    public void moneyAddFormatExceptionNegativeNumberInt() {
        String userInput  = "-15";
        ByteArrayInputStream inputStream = new ByteArrayInputStream(userInput .getBytes());
        System.setIn(inputStream);
        ATM atm = new ATM();
        BigDecimal actual = atm.moneyAdd();
    }

    @Test (expected = IllegalArgumentException.class)
    public void moneyAddFormatExceptionNegativeNumberDouble() {
        String userInput  = "-23.1";
        ByteArrayInputStream inputStream = new ByteArrayInputStream(userInput .getBytes());
        System.setIn(inputStream);
        ATM atm = new ATM();
        BigDecimal actual = atm.moneyAdd();
    }

    @Test
    public void moneySubtractIntCorrectly() {
        String userInput  = "50";
        ByteArrayInputStream inputStream = new ByteArrayInputStream(userInput .getBytes());
        System.setIn(inputStream);
        ATM atm = new ATM();
        BigDecimal actual = atm.moneySubtract(new BigDecimal("100.1"));
        int index = new BigDecimal(50).compareTo(actual);
        Assert.assertEquals(0,index);
    }

    @Test
    public void moneySubtractDoubleCorrectly() {
        String userInput  = "50.1";
        ByteArrayInputStream inputStream = new ByteArrayInputStream(userInput .getBytes());
        System.setIn(inputStream);
        ATM atm = new ATM();
        BigDecimal actual = atm.moneySubtract(new BigDecimal("100.1"));
        int index = new BigDecimal("50.1").compareTo(actual);
        Assert.assertEquals(0,index);
    }

    @Test (expected = IllegalArgumentException.class)
    public void moneySubtractFormatExceptionString() {
        String userInput  = "vadim";
        ByteArrayInputStream inputStream = new ByteArrayInputStream(userInput .getBytes());
        System.setIn(inputStream);
        ATM atm = new ATM();
        BigDecimal actual = atm.moneySubtract(new BigDecimal("100.1"));
    }

    @Test (expected = IllegalArgumentException.class)
    public void moneySubtractFormatExceptionNegativeNumberInt() {
        String userInput  = "-15";
        ByteArrayInputStream inputStream = new ByteArrayInputStream(userInput .getBytes());
        System.setIn(inputStream);
        ATM atm = new ATM();
        BigDecimal actual = atm.moneySubtract(new BigDecimal("100.1"));
    }

    @Test (expected = IllegalArgumentException.class)
    public void moneySubtractFormatExceptionNegativeNumberDouble() {
        String userInput  = "-23.1";
        ByteArrayInputStream inputStream = new ByteArrayInputStream(userInput .getBytes());
        System.setIn(inputStream);
        ATM atm = new ATM();
        BigDecimal actual = atm.moneySubtract(new BigDecimal("100.1"));
    }

    @Test (expected = IllegalArgumentException.class)
    public void moneySubtractFormatExceptionNegativeNotEnoughMoney() {
        String userInput  = "100";
        ByteArrayInputStream inputStream = new ByteArrayInputStream(userInput .getBytes());
        System.setIn(inputStream);
        ATM atm = new ATM();
        BigDecimal actual = atm.moneySubtract(new BigDecimal("99"));
    }

    @Test
    public void moneyTransferIntCorrectly() {
        String userInput  = "50";
        ByteArrayInputStream inputStream = new ByteArrayInputStream(userInput .getBytes());
        System.setIn(inputStream);
        ATM atm = new ATM();
        BigDecimal actual = atm.moneyTransfer();
        int index = new BigDecimal(50).compareTo(actual);
        Assert.assertEquals(0,index);
    }

    @Test
    public void moneyTransferDoubleCorrectly() {
        String userInput  = "455.1";
        ByteArrayInputStream inputStream = new ByteArrayInputStream(userInput .getBytes());
        System.setIn(inputStream);
        ATM atm = new ATM();
        BigDecimal actual = atm.moneyTransfer();
        int index = new BigDecimal("455.1").compareTo(actual);
        Assert.assertEquals(0,index);
    }

    @Test (expected = IllegalArgumentException.class)
    public void moneyTransferFormatExceptionString() {
        String userInput  = "vadimMilko";
        ByteArrayInputStream inputStream = new ByteArrayInputStream(userInput .getBytes());
        System.setIn(inputStream);
        ATM atm = new ATM();
        BigDecimal actual = atm.moneyTransfer();
    }

    @Test (expected = IllegalArgumentException.class)
    public void moneyTransferFormatExceptionNegativeNumberInt() {
        String userInput  = "-17";
        ByteArrayInputStream inputStream = new ByteArrayInputStream(userInput .getBytes());
        System.setIn(inputStream);
        ATM atm = new ATM();
        BigDecimal actual = atm.moneyTransfer();
    }

    @Test (expected = IllegalArgumentException.class)
    public void moneyTransferFormatExceptionNegativeNumberDouble() {
        String userInput  = "-25.1";
        ByteArrayInputStream inputStream = new ByteArrayInputStream(userInput .getBytes());
        System.setIn(inputStream);
        ATM atm = new ATM();
        BigDecimal actual = atm.moneyTransfer();
    }
}