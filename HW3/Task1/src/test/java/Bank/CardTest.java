package Bank;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class CardTest {

    @Test
    public void currencyTransferDoubleToDouble() {
        Card card = new Card("Vadim",new BigDecimal("100.11"));
        BigDecimal actual = card.currencyTransfer(new BigDecimal("100.11"));
        int index = actual.compareTo(new BigDecimal("1.0000"));
        Assert.assertEquals(0,index);
    }

    @Test
    public void currencyTransferIntToDouble() {
        Card card = new Card("Vadim",new BigDecimal(100));
        BigDecimal actual = card.currencyTransfer(new BigDecimal("10.1"));
        int index = actual.compareTo(new BigDecimal("9.9009"));
        Assert.assertEquals(0,index);
    }

    @Test
    public void currencyTransferIntToInt() {
        Card card = new Card("Vadim",new BigDecimal(100));
        BigDecimal actual = card.currencyTransfer(new BigDecimal(10));
        int index = actual.compareTo(new BigDecimal("10.0000"));
        Assert.assertEquals(0,index);
    }

    @Test
    public void currencyTransferDoubleToInt() {
        Card card = new Card("Vadim",new BigDecimal("54.12"));
        BigDecimal actual = card.currencyTransfer(new BigDecimal(14));
        int index = actual.compareTo(new BigDecimal("3.8657"));
        Assert.assertEquals(0,index);
    }

    @Test
    public void balanceWithdrawalDouble() {
        Card card=new Card("Vadim",new BigDecimal("1000.11"));
        BigDecimal actual = card.balanceWithdrawal();
        int index = actual.compareTo(new BigDecimal("1000.11"));
        Assert.assertEquals(0,index);
    }

    @Test
    public void balanceWithdrawalInt() {
        Card card=new Card("Vadim",new BigDecimal(10));
        BigDecimal actual = card.balanceWithdrawal();
        int index = actual.compareTo(new BigDecimal(10));
        Assert.assertEquals(0,index);
    }

    @Test
    public void balanceWithdrawalZero() {
        Card card=new Card("Vadim",new BigDecimal(0));
        BigDecimal actual = card.balanceWithdrawal();
        int index = actual.compareTo(new BigDecimal(0));
        Assert.assertEquals(0,index);
    }

    @Test
    public void cashWithdrawalInt() {
        Card card = new Card("Vadim",new BigDecimal(1024));
        card.cashWithdrawal(new BigDecimal(212));
        BigDecimal actual = card.balanceWithdrawal();
        int index = actual.compareTo(new BigDecimal(812));
        Assert.assertEquals(0,index);
    }
    @Test
    public void cashWithdrawalDouble() {
        Card card = new Card("Vadim",new BigDecimal(259));
        card.cashWithdrawal(new BigDecimal("145.234"));
        BigDecimal actual = card.balanceWithdrawal();
        int index = actual.compareTo(new BigDecimal("113.766"));
        Assert.assertEquals(0,index);
    }

    @Test
    public void addToBalanceInt() {
        Card card = new Card("Vadim",new BigDecimal(100));
        card.addToBalance(new BigDecimal(47));
        BigDecimal actual = card.balanceWithdrawal();
        int index = actual.compareTo(new BigDecimal(147));
        Assert.assertEquals(0,index);
    }

    @Test
    public void addToBalanceDouble() {
        Card card = new Card("Vadim",new BigDecimal(100));
        card.addToBalance(new BigDecimal("47.145"));
        BigDecimal actual = card.balanceWithdrawal();
        int index = actual.compareTo(new BigDecimal("147.145"));
        Assert.assertEquals(0,index);
    }
}