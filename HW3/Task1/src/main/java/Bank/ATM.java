package Bank;
import java.math.BigDecimal;
import java.util.Scanner;

public class ATM {

    public BigDecimal moneyAdd(){
        System.out.print("Введите количество денег которое вы хотите зачислить = ");
        BigDecimal callMoney = scanner();
        if(callMoney==null){
            throw new IllegalArgumentException("Exception format");
        }
        if(formatException(callMoney)){
            throw new IllegalArgumentException("Exception format");
        }
        return callMoney;
    }

    public BigDecimal moneySubtract(BigDecimal nowCardBalance){
        System.out.print("Введите количество денег которое вы хотите снять = ");
        BigDecimal callMoney = scanner();
        if(callMoney==null){
            throw new IllegalArgumentException("Exception format");
        }
        if(formatException(callMoney) || formatException(nowCardBalance.subtract(callMoney))){
            throw new IllegalArgumentException("Exception format");
        }
        return callMoney;
    }

    public BigDecimal moneyTransfer(){
        System.out.print("Введите курс по которому вы хотите первести деньги = ");
        BigDecimal callMoney = scanner();
        if(callMoney==null){
            throw new IllegalArgumentException("Exception format");
        }
        if(formatException(callMoney)){
            throw new IllegalArgumentException("Exception format");
        }
        return callMoney;
    }
    private boolean formatException(BigDecimal callMoney){
        return callMoney.signum() == -1;
    }
    private BigDecimal scanner(){
        Scanner scanner = new Scanner(System.in);
        try {
            return new BigDecimal(scanner.next());
        }
        catch (Exception e){
            System.out.println(e.getMessage());
            return null;
        }


    }
}
