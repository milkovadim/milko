package Bank;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Card {
    private String nameCardHolder;
    private BigDecimal cardBalance;

    public Card(String nameCardHolder,BigDecimal cardBalance){
        try {
            this.nameCardHolder = nameCardHolder;
            this.cardBalance = cardBalance;
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public Card (String nameCardHolder){
        this.nameCardHolder = nameCardHolder;
        this.cardBalance = new BigDecimal(0);
    }

    public BigDecimal currencyTransfer(BigDecimal exchangeRate){
        return cardBalance.divide(exchangeRate,4,RoundingMode.DOWN);
    }

    public BigDecimal balanceWithdrawal(){
        return cardBalance;
    }

    public void cashWithdrawal(BigDecimal cash){
        cardBalance = cardBalance.subtract(cash);
    }
    public void addToBalance(BigDecimal addCash){
        cardBalance = cardBalance.add(addCash);
    }
}
