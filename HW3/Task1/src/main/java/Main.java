import Bank.ATM;
import Bank.Card;

import java.math.BigDecimal;

public class Main {
    public static void main(String[] args) {
        try {
            ATM atm= new ATM();
            Card card1 = new Card(args[0],new BigDecimal(args[1]));
            Card card2 = new Card(args[0]);
            System.out.println("Количество денег на счете = " + card1.balanceWithdrawal());
            card1.addToBalance(atm.moneyAdd());
            System.out.println("Количество денег на счете после зачисления = " + card1.balanceWithdrawal());
            card1.cashWithdrawal(atm.moneySubtract(card1.balanceWithdrawal()));
            System.out.println("Количество денег на счете после снятия средств = "+ card1.balanceWithdrawal());
            System.out.println("Деньги в конвертированном виде = " + card1.currencyTransfer(atm.moneyTransfer()));
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
