public class Menu {
    private int algorithmParameters;
    private int loopTypeParameters;
    private int variable;

    public Menu(String[] args) {
        exceptionMethod(args);
    }
    private void exceptionMethod(String[] parameters){
        if(!parseParameters(parameters)){
            throw new NumberFormatException("Wrong format");
        }
        if(!parametersValid()){
            throw new IllegalArgumentException("Variable entered incorrectly");
        }
    }

    private boolean parametersValid(){
        if(algorithmParameters>2 || algorithmParameters<=0){
            return false;
        }
        if(loopTypeParameters>3 || loopTypeParameters<=0){
            return false;
        }
        if(variable<=0){
            return false;
        }
        return true;
    }
    private boolean parseParameters(String[] parameters){
        try {
            algorithmParameters = Integer.parseInt(parameters[0]);
            loopTypeParameters = Integer.parseInt(parameters[1]);
            variable = Integer.parseInt(parameters[2]);
        }
        catch (Exception e){
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }

    public String selectionСase(){
        CalculatorFibonachi fibonacci = new CalculatorFibonachi(variable);
        CalculatorFactorial factorial = new CalculatorFactorial(variable);
        String results;
        switch (algorithmParameters){
            case 1 -> {
                switch (loopTypeParameters) {
                    case 1 -> results = "Fibonacci series : " + fibonacci.methodWhile();
                    case 2 -> results = "Fibonacci series : " + fibonacci.methodDoWhile();
                    case 3 -> results = "Fibonacci series : " + fibonacci.methodFor();
                    default -> throw new IllegalStateException("Unexpected value: " + loopTypeParameters);
                }
            }
            case 2 -> {
                switch (loopTypeParameters) {
                    case 1 -> results = "Factorial "+variable+" = "+factorial.methodWhile();
                    case 2 -> results = "Factorial "+variable+" = "+factorial.methodDoWhile();
                    case 3 -> results = "Factorial "+variable+" = "+factorial.methodFor();
                    default -> throw new IllegalStateException("Unexpected value: " + loopTypeParameters);
                }
            }
            default ->
                    throw new IllegalStateException("Unexpected value: " + algorithmParameters);
        }
        return results;
    }
}
