import java.util.ArrayList;

public class CalculatorFibonachi {
    ArrayList<Integer> fibonacciNumbers = new ArrayList<Integer>();
    int variable;

    public CalculatorFibonachi(int variable){
        fibonacciNumbers.add(0);
        fibonacciNumbers.add(1);
        this.variable=variable;
    }
    public String methodWhile(){
        int i=2;
        while (i<variable) {
            fibonacciNumbers.add(fibonacciNumbers.get(i-1)+fibonacciNumbers.get(i-2));
            i++;
        }
        return parseToString(variable);
    }
    public String methodFor(){
        for(int i=2;i<variable;i++){
            fibonacciNumbers.add(fibonacciNumbers.get(i-1)+fibonacciNumbers.get(i-2));
        }
        return parseToString(variable);
    }
    public String methodDoWhile() {
        int i = 2;
        do {
            fibonacciNumbers.add(fibonacciNumbers.get(i - 1) + fibonacciNumbers.get(i - 2));
            i++;
        } while (i < variable);
        return parseToString(variable);
    }
    private String parseToString(int variable){
        String results ="";
        for (int i=0;i<variable;i++){
            results+=fibonacciNumbers.get(i)+" ";
        }
        return results;
    }
}
