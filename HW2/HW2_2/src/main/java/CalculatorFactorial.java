public class CalculatorFactorial {
    private long variable;
    public CalculatorFactorial(long parameters){
        variable=parameters;
    }
    public String methodFor(){
        long results=1;
        for(int i=2;i<=variable;i++) {
            results *= i;
        }
        return toString(results);
    }
    public String methodWhile(){
        long results=1;
        int i=2;
        while(i<=variable){
            results *= i;
            i++;
        }
        return toString(results);
    }

    private String toString(long results) {
        String resultsString = "" + results;
        return resultsString;
    }

    public  String methodDoWhile(){
        long results=1;
        int i=2;
        do{
            results *= i;
            i++;
        }while(i<=variable);
        return toString(results);
    }
}
