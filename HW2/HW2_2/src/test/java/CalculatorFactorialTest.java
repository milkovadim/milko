import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CalculatorFactorialTest {

   final long veriablePositivTest = 10;
    @Test
    void methodForTest() {
        CalculatorFactorial factorialTest= new CalculatorFactorial(veriablePositivTest);
        String actual = factorialTest.methodFor();
        assertEquals("3628800",actual);
    }

    @Test
    void methodWhileTest() {
        CalculatorFactorial factorialTest= new CalculatorFactorial(veriablePositivTest);
        String actual = factorialTest.methodFor();
        assertEquals("3628800",actual);
    }

    @Test
    void methodDoWhileTest() {
        CalculatorFactorial factorialTest= new CalculatorFactorial(veriablePositivTest);
        String actual = factorialTest.methodFor();
        assertEquals("3628800",actual);
    }

}
