import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorFibonachiTest {
    final int veriablePositivTest = 10;
    @Test
    public void methodWhileTest() {
        CalculatorFibonachi fibonachiTest= new CalculatorFibonachi(veriablePositivTest);
        String actual = fibonachiTest.methodFor();
        assertEquals("0 1 1 2 3 5 8 13 21 34 ",actual);
    }

    @Test
    public void methodForTest() {
        CalculatorFibonachi fibonachiTest= new CalculatorFibonachi(veriablePositivTest);
        String actual = fibonachiTest.methodFor();
        assertEquals("0 1 1 2 3 5 8 13 21 34 ",actual);
    }

    @Test
    public void methodDoWhileTest() {
        CalculatorFibonachi fibonachiTest= new CalculatorFibonachi(veriablePositivTest);
        String actual = fibonachiTest.methodDoWhile();
        assertEquals("0 1 1 2 3 5 8 13 21 34 ",actual);
    }
}