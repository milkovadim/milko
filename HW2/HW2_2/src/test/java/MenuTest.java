import org.junit.Test;

import static org.junit.Assert.*;

public class MenuTest {
    String[] TestArgsFactorial = {"2","3","10"};
    String[] TestArgsFibonachi = {"1","2","10"};
    String[] negativTestArgsWrongFormat = {"2.1","3","10"};
    String[] negativTestArgsVariableEnteredIncorrectly = {"2","3","-5"};
    @Test
    public void selectionСaseTestPositivFactorial() {
        Menu menu=new Menu(TestArgsFactorial);
        String actual = menu.selectionСase();
        assertEquals("Factorial 10 = 3628800",actual);
    }
    @Test
    public void selectionСaseTestPositivFibonachi() {
        Menu menu=new Menu(TestArgsFibonachi);
        String actual = menu.selectionСase();
        assertEquals("Fibonacci series : 0 1 1 2 3 5 8 13 21 34 ",actual);
    }
    @Test (expected = NumberFormatException.class)
    public void selectionСaseTestWrongFormat() {
        Menu menu=new Menu(negativTestArgsWrongFormat);
        String actual = menu.selectionСase();
    }
    @Test (expected = IllegalArgumentException.class)
    public void selectionСaseTestVariableEnteredIncorrectly() {
        Menu menu=new Menu(negativTestArgsVariableEnteredIncorrectly);
        String actual = menu.selectionСase();
    }
}