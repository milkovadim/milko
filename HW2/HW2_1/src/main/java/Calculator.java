public class Calculator {
    private int a;
    private int p;
    private double m1;
    private double m2;

    public Calculator(String[] args) {
        exceptionMethod(args);
    }

    private void exceptionMethod(String[] parameters){
        if(!parseParameters(parameters)){
            throw new NumberFormatException("Wrong format");
        }
        if(!parametersValid()){
            throw new IllegalArgumentException("Variable entered incorrectly");
        }
    }
     private boolean parametersValid(){
        if(p==0){
            return false;
        }
        if(m1+m2==0){
            return false;
        }
        return true;
     }
    public double calculatingG(){
        return 4 * Math.PI*(Math.pow(a,3)/ (Math.pow(p,2)*(m1+m2)));
    }
    private boolean parseParameters(String [] parameters){
        try {
            a = Integer.parseInt(parameters[0]);
            p = Integer.parseInt(parameters[1]);
            m1 = Double.parseDouble(parameters[2]);
            m2 = Double.parseDouble(parameters[3]);
        }
        catch (Exception e){
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }
}
