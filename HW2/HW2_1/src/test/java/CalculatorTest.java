import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorTest {
    String[] positivTestArgs = {"3","2","3.5","5.6"};
    String[] negativTestArgsWrongFormat = {"15.6","2","3.5","5.6"};
    String[] negativTestArgsVariableEnteredIncorrectly = {"3","0","3.5","5.6"};
    @Test
    public void calculatingGPositivTest() {
        Calculator calculator=new Calculator(positivTestArgs);
        double actual = calculator.calculatingG();
        assertEquals(9.321208972189497,actual,0.000005);
    }
    @Test (expected = NumberFormatException.class)
    public void calculatingGTestWrongFormat(){
        Calculator calculator=new Calculator(negativTestArgsWrongFormat);
        double actual = calculator.calculatingG();
    }
    @Test (expected = IllegalArgumentException.class)
    public void calculatingGTestVariableEnteredIncorrectly(){
        Calculator calculator=new Calculator(negativTestArgsVariableEnteredIncorrectly);
        double actual = calculator.calculatingG();
    }
}